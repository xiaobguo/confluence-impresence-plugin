/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.impresence2.reporter;

import static org.apache.commons.lang.StringUtils.isNotBlank;

import java.io.IOException;

import com.atlassian.renderer.v2.RenderUtils;

/**
 * TODO: Document this class.
 * <p/>
 * User: david
 * Date: Jun 19, 2006
 * Time: 5:31:25 PM
 */
public class SkypePresenceReporter extends LocaleAwarePresenceReporter implements PresenceReporter
{

    public static final String KEY = "skype";

    public String getKey()
    {
        return KEY;
    }

    public String getName()
    {
        return getText("presencereporter.skype.name");
    }

    public String getServiceHomepage()
    {
        return getText("presencereporter.skype.servicehomepage");
    }

    public boolean hasConfig()
    {
        return false;
    }

    public boolean requiresConfig()
    {
        return false;
    }

    public String getPresenceXHTML(String id, boolean outputId) throws IOException, PresenceException
    {
        if (!isNotBlank(id))
        {
            return RenderUtils.error(getText("presencereporter.skype.error.noscreenname"));
        }
        else
        {
            StringBuffer out = new StringBuffer();
            out.append("<script type='text/javascript' src='http://download.skype.com/share/skypebuttons/js/skypeCheck.js'></script>")
               .append("<a href='skype:").append(id).append("?call'>")
               .append("<img src='http://mystatus.skype.com/smallclassic/").append(id).append("' style='border: none;' align='absmiddle' alt='My status' />")
               .append("</a>");

            if (outputId)
            {
                out.append("&nbsp;<a href='skype:").append(id).append("?call'>")
                   .append(id)
                   .append("</a>");
            }

            return out.toString();
        }
    }
}
