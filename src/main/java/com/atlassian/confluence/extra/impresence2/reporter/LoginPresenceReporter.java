/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.impresence2.reporter;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import org.apache.commons.lang.StringUtils;

/**
 * TODO: Document this class.
 * <p/>
 * User: david
 * Date: Jun 19, 2006
 * Time: 5:52:25 PM
 */
public abstract class LoginPresenceReporter extends LocaleAwarePresenceReporter implements PresenceReporter
{
    private static final String ID_PREFIX = "extra.im.account.";
    private static final String PASSWORD_PREFIX = "extra.im.password.";

    private BandanaManager bandanaManager;
    private BootstrapManager bootstrapManager;

    public String getId()
    {
        return (String) bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, ID_PREFIX + getKey());
    }

    public void setId(String id)
    {
        bandanaManager.setValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, ID_PREFIX + getKey(), StringUtils.defaultString(StringUtils.trim(id)));
    }

    public String getPassword()
    {
        return (String) bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, PASSWORD_PREFIX + getKey());
    }

    public void setPassword(String password)
    {
        bandanaManager.setValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, PASSWORD_PREFIX + getKey(), StringUtils.defaultString(StringUtils.trim(password)));
    }

    public boolean hasConfig()
    {
        return true;
    }

    public boolean requiresConfig()
    {
        return StringUtils.isBlank(getId()) || StringUtils.isBlank(getPassword());
    }

    protected String getPresenceLink(String id, String icon, String status, boolean outputId)
    {
        String url = getPresenceURL(id);

        StringBuffer out = new StringBuffer();
        if (url != null)
        {
            out.append("<A href='").append(url).append("'>");
        }

        out.append("<img src='").append(bootstrapManager.getWebAppContextPath())
            .append("/download/resources/confluence.extra.impresence2:im/images/").append(icon).append(".gif'")
            .append(" style='vertical-align:bottom; margin:0px 1px;' border='0'")
            .append("' title='").append(status).append("'")
            .append("/>");

        if (url != null)
        {
            out.append("</a>");
        }

        if (outputId)
        {
            out.append("&nbsp;");

            if (url != null)
            {
                out.append("<A href='").append(url).append("' title='").append(status).append("'>");
            }

            out.append(id);

            if (url != null)
            {
                out.append("</a>");
            }
        }

        return out.toString();
    }

    /**
     * Returns the URL to link to the specified ID.
     *
     * @param id The ID to link to.
     * @return The URL link for the presence status
     */
    protected abstract String getPresenceURL(String id);

    public void setBootstrapManager(BootstrapManager bootstrapManager)
    {
        this.bootstrapManager = bootstrapManager;
    }

    public void setBandanaManager(BandanaManager bandanaManager)
    {
        this.bandanaManager = bandanaManager;
    }
}
