package com.atlassian.confluence.extra.impresence2.reporter;

import com.atlassian.confluence.extra.impresence2.util.LocaleSupport;

import java.util.List;

public abstract class LocaleAwarePresenceReporter implements PresenceReporter 
{
    private LocaleSupport localeSupport;

    public LocaleSupport getLocaleSupport() 
    {
        return localeSupport;
    }

    public void setLocaleSupport(LocaleSupport localeSupport) 
    {
        this.localeSupport = localeSupport;
    }

    public String getText(String key) 
    {
        return getLocaleSupport().getText(key);
    }

    public String getText(String key, Object[] substitutions) 
    {
        return getLocaleSupport().getText(key, substitutions);
    }

    public String getText(String key, List list) 
    {
        return getLocaleSupport().getText(key, list);
    }

    public String getTextStrict(String key) 
    {
        return getLocaleSupport().getTextStrict(key);
    }
}
