/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.impresence2.reporter;

import static org.apache.commons.lang.StringUtils.isNotBlank;

import java.io.IOException;

import com.atlassian.renderer.v2.RenderUtils;

/**
 * Provides the content for AIM presence indication.
 */
public class AIMPresenceReporter extends LocaleAwarePresenceReporter
{

    public static final String KEY = "aim";

    public String getKey()
    {
        return KEY;
    }

    public String getName()
    {
        return getText("presencereporter.aim.name");
    }

    public String getServiceHomepage()
    {
        return getText("presencereporter.aim.servicehomepage");
    }

    public boolean hasConfig()
    {
        return false;
    }

    public boolean requiresConfig()
    {
        return false;
    }

    public String getPresenceXHTML(String id, boolean outputId) throws IOException, PresenceException
    {
        if (!isNotBlank(id))
        {
            return RenderUtils.error(getText("presencereporter.aim.error.noscreenname"));
        }
        else
        {
            return "<a href='aim:GoIM?screenname=" + id + "'><img src='http://api.oscar.aol.com/SOA/key=jo1rkjdnQ-LEjx49/presence/" + id + "' height='16' width='16' style='vertical-align:bottom; margin:0px 1px;' border='0'/></a>" +
                (outputId ? "&nbsp;<a title=\"AIM " + id + "\" href=\"aim:GoIM?screenname=" + id + "\">" + id + "</a>" : "");
        }
    }
}
