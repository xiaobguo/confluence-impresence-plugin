package com.atlassian.confluence.extra.impresence2.reporter;

import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;

import static org.mockito.Mockito.when;

public abstract class AbstractLoginPresenceReporterTest<T extends PresenceReporter>
        extends AbstractPresenceReporterTest<T>
{

    static final String ID_PREFIX = "extra.im.account.";

    static final String PASSWORD_PREFIX = "extra.im.password.";

    public void testRequiresConfigWhenConfiguredConfiguredAccountIdIsNull()
    {
        when(bandanaManager.getValue(ConfluenceBandanaContext.GLOBAL_CONTEXT, ID_PREFIX + getPresenceReporterKey())).thenReturn(null);
        assertTrue(createPresenceReporter().requiresConfig());
    }

    public void testRequiresConfigWhenConfiguredAccountPasswordIsNull()
    {
        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                PASSWORD_PREFIX + getPresenceReporterKey()
        )).thenReturn(null);

        assertTrue(createPresenceReporter().requiresConfig());
    }

    public void testRequiresConfigWhenConfiguredAccountCredentialsAreNotNull()
    {
        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                ID_PREFIX + getPresenceReporterKey()
        )).thenReturn("username");

        when(bandanaManager.getValue(
                ConfluenceBandanaContext.GLOBAL_CONTEXT,
                PASSWORD_PREFIX + getPresenceReporterKey()
        )).thenReturn("password");

        assertFalse(createPresenceReporter().requiresConfig());
    }
}