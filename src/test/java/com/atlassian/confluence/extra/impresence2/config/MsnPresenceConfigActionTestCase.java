package com.atlassian.confluence.extra.impresence2.config;

import com.atlassian.confluence.extra.impresence2.reporter.MSNPresenceReporter;

public class MsnPresenceConfigActionTestCase extends LoginPresenceConfigActionTestCase<MSNPresenceConfigAction, MSNPresenceReporter>
{
    protected MSNPresenceConfigAction createAction()
    {
        MSNPresenceConfigAction action = new MSNPresenceConfigAction()
        {
            public String getText(String key, Object[] substitute)
            {
                return key;
            }

            protected String getServiceName()
            {
                return "MSN Messenger";
            }
        };

        action.setPresenceManager(presenceManager);
        return action;
    }

    protected String getServiceKey()
    {
        return "msn";
    }

    protected Class<MSNPresenceReporter> getPresenceReporterClass()
    {
        return MSNPresenceReporter.class;
    }
}
