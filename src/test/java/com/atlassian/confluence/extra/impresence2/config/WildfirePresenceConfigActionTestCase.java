package com.atlassian.confluence.extra.impresence2.config;

import com.atlassian.confluence.extra.impresence2.reporter.WildfirePresenceReporter;

public class WildfirePresenceConfigActionTestCase extends ServerPresenceConfigActionTestCase<WildfirePresenceConfigAction, WildfirePresenceReporter>
{
    protected WildfirePresenceConfigAction createAction()
    {
        WildfirePresenceConfigAction action = new WildfirePresenceConfigAction()
        {
            protected String getServiceName()
            {
                return "Wildfire";
            }

            public String getText(String key, Object[] substitute)
            {
                return key;
            }
        };

        action.setPresenceManager(presenceManager);
        return action;
    }

    protected String getServiceKey()
    {
        return "wildfire";
    }

    protected Class<WildfirePresenceReporter> getPresenceReporterClass()
    {
        return WildfirePresenceReporter.class;
    }
}
