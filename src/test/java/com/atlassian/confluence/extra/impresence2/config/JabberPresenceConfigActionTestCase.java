package com.atlassian.confluence.extra.impresence2.config;

import com.atlassian.confluence.extra.impresence2.reporter.JabberPresenceReporter;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class JabberPresenceConfigActionTestCase extends LoginPresenceConfigActionTestCase<JabberPresenceConfigAction, JabberPresenceReporter>
{
    protected JabberPresenceConfigAction createAction()
    {
        JabberPresenceConfigAction action = new JabberPresenceConfigAction()
        {
            public String getText(String key, Object[] substitute)
            {
                return key;
            }

            protected String getServiceName()
            {
                return "Jabber (XMPP)";
            }
        };

        action.setPresenceManager(presenceManager);
        return action;
    }

    protected String getServiceKey()
    {
        return "jabber";
    }

    protected Class<JabberPresenceReporter> getPresenceReporterClass()
    {
        return JabberPresenceReporter.class;
    }

    public void testExecuteWhenDomainIsNotSpecified() throws Exception
    {
        action.setDomain(null);
        when(presenceReporter.getDomain()).thenReturn("fakeDomain");
        assertEquals("success", action.execute());
        verify(presenceReporter).setDomain(null);
    }

    public void testExecuteWhenDomainIsSpecified() throws Exception
    {
        action.setDomain("newFakeDomain");
        assertEquals("success", action.execute());
        verify(presenceReporter).setDomain("newFakeDomain");
    }

    public void testExecuteWhenPortIsNotSpecified() throws Exception
    {
        action.setPort(null);
        when(presenceReporter.getPort()).thenReturn(-1);
        assertEquals("success", action.execute());
        verify(presenceReporter).setPort(null);
    }

    public void testExecuteWhenPortIsSpecified() throws Exception
    {
        action.setPort("5222");
        assertEquals("success", action.execute());
        verify(presenceReporter).setPort(5222);
    }

    public void testValidateWhenPortIsNonNumeric() throws Exception
    {
        action.setPort("bad-port");
        action.validate();
        assertEquals(1, action.getActionErrors().size());
    }

    public void testValidateWhenPortIsNegative() throws Exception
    {
        action.setPort("-5222");
        action.validate();
        assertEquals(1, action.getActionErrors().size());
    }
}
