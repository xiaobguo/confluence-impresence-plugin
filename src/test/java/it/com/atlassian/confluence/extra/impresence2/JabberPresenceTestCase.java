package it.com.atlassian.confluence.extra.impresence2;

import com.atlassian.confluence.extra.impresence2.reporter.JabberPresenceReporter;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;
import org.xml.sax.SAXException;

public class JabberPresenceTestCase extends AbstractPresenceTestCase
{
    protected void setUp() throws Exception {
        super.setUp();
        assertTrue(getBandanaHelper("extra.im.account.jabber").delete());
        assertTrue(getBandanaHelper("extra.im.password.jabber").delete());
    }

    protected boolean requiresConfiguration()
    {
        return true;
    }

    public void testRequiresConfig()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Requires Configuration");
        pageHelper.setContent(
                "{im:john.doe@localhost.localdomain|service=jabber}"
        );

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertTextPresent("An administrator must configure your server's Jabber (XMPP) Service before you can use this macro.");
        assertLinkPresentWithText("Configure Jabber (XMPP) Service");
    }

    public void testConfigurationNotAllowedForNonAdminUsers()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Dummy Account Configuration Accessible By Admins Only");
        pageHelper.setContent(
                "{im:john.doe@localhost.localdomain|service=jabber}"
        );

        assertTrue(pageHelper.create());

        logout();
        login(nonAdminUserName, nonAdminPassword);

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertTextPresent("An administrator must configure your server's Jabber (XMPP) Service before you can use this macro.");
        assertLinkNotPresentWithText("Configure Jabber (XMPP) Service");
        
        logout();
        loginAsAdmin(); /* So tearDown can be called successfully */
    }

    private void configureDummyAccount(final String reporterId, final String reporterPassword)
    {
        setWorkingForm("presenceconfigform");
        setTextField("reporterId", reporterId);
        setTextField("reporterPassword", reporterPassword);
        submit("update");

        /* Assert if the values are correctly set */
        assertTitleEquals("Configure the Jabber (XMPP) Service - Confluence");
        assertTableEquals("presenceconfigstatetable",
                new String[][] {
                        new String[] { "Current ID:", reporterId },
                        new String[] { "Current Password:", reporterPassword },
                        new String[] { "Current Domain:", "chat.example.com" },
                        new String[] { "Current Port:", Integer.toString(JabberPresenceReporter.DEFAULT_JABBER_PORT) },
                });

        assertEquals(reporterId, getElementAttributByXPath("//input[@name='reporterId']", "value"));
        assertEquals(reporterPassword, getElementAttributByXPath("//input[@name='reporterPassword']", "value"));
    }

    public void testShowPresenceWithId() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Test Show Presence With ID");
        pageHelper.setContent(
                "{im:john.doe@localhost.localdomain|service=jabber}"
        );

        assertTrue(pageHelper.create());

        try
        {
        	// Configure Jabber Service requires escalated privileges
        	gotoPageWithEscalatedPrivileges("/pages/viewpage.action?pageId=" + pageHelper.getId());

        	clickLinkWithText("Configure Jabber (XMPP) Service");

        	assertTitleEquals("Configure the Jabber (XMPP) Service - Confluence");
        	assertTableEquals("presenceconfigstatetable",
        			new String[][] {
                    new String[] { "Current ID:", "None!" },
                    new String[] { "Current Password:", "None!" },
                    new String[] { "Current Domain:", "chat.example.com" },
                    new String[] { "Current Port:", Integer.toString(JabberPresenceReporter.DEFAULT_JABBER_PORT) },
        	});
        	assertEquals("", getElementAttributByXPath("//input[@name='reporterId']", "value"));
        	assertEquals("", getElementAttributByXPath("//input[@name='reporterPassword']", "value"));

        	configureDummyAccount("admin@localhost.localdomain", "admin");

        	/* Now, let's see if the page shows the presence of the targeted user */
        	gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

            assertTextNotPresent("An administrator must configure your server's Jabber (XMPP) Service before you can use this macro.");
        	/* Well, at least we know the configuration is good... */
        	assertElementNotPresentByXPath("//div[@class='wiki-content']//div[@class='errorBox']");
        }
        finally
        {
        	dropEscalatedPrivileges();
        }
    }
}
